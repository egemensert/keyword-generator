import json
import glob
import string

import argparse
import jellyfish as jf
from unidecode import unidecode
from gensim.models import Word2Vec, FastText

from multiprocessing import cpu_count

import utils as U


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                    prog = 'KeywordBulucu',
                    description = 'Postgres tweet dump ile word embeddingsi kullanarak keyword bulur.',
            )

    parser.add_argument('--keywords', type=str, default="erzak")
    parser.add_argument('--input_filepath', type=str)
    parser.add_argument('--output_filepath', default='kelime_onerileri.json', type=str)
    parser.add_argument('--model_path', default=False, type=str)
    parser.add_argument('--model_type', choices=['word2vec', 'fasttext'], default='word2vec', required=True) # cbow vs takılmıyorum
    parser.add_argument('--model_output_path', default='keyword_modeli', type=str)
    parser.add_argument('--epochs', default=50, type=int)
    parser.add_argument('--min_count', default=1, type=int)
    parser.add_argument('--window', default=5, type=int)
    parser.add_argument('--vector_size', default=100, type=int)
    parser.add_argument('--ngrams', default=2, type=int)
    parser.add_argument('--ngrams_min_count', default=1, type=int)
    parser.add_argument('--ngrams_threshold', default=2, type=int)
    parser.add_argument('--topn', default=100, type=int)
    parser.add_argument('--seed', default=2023, type=int)

    args = parser.parse_args()

    if args.model_type == 'word2vec':
        Model = Word2Vec
    elif args.model_type == 'fasttext':
        Model = FastText
    else:
        raise Exception(f'Beklenmedik model türü: {args.model_type}')

    if args.model_path:
        model = Model.load(args.input_filepath)
    elif args.input_filepath:
        print('Yeni model eğitiliyor.')
        corpus = U.load_corpus(args.input_filepath, ngrams=args.ngrams, min_count=args.ngrams_min_count, threshold=args.ngrams_threshold)

        print('Model eğitiliyor.')
        model = Model(corpus, epochs=args.epochs, min_count=args.min_count, window=args.window, vector_size=args.vector_size, workers=cpu_count())

        print(f'Eğitilmiş model {args.model_output_path} adresine kaydediliyor.')
        model.save(args.model_output_path)
    else:
        raise Exception('Eğer eğitilmiş bir model varsa model_path değişkeni aracılığıyla modeli belirtin. Yoksa lütfen modelin eğitileceği Postgres dump\'ın ikinci argüman olarak sağlayın')

    seed_keywords = []
    for keyword in args.keywords.split(','):
        seed_keywords.append(keyword.strip())


    output = []
    for keyword in seed_keywords:
        output.append(U.get_similars(keyword, model, args.topn))
    
    with open(args.output_filepath, 'w', encoding='utf-8') as f:
        f.write(json.dumps(output, indent=2, ensure_ascii=False))
    


