import os
import json
import string
import jellyfish as jf

from unidecode import unidecode
from gensim.models import Phrases
from gensim.models.phrases import Phraser

def preprocess(tweet, keep_lodash=False):
    ''' tweeti ascii'ye normalize eder ve noktalama işaretleri ile sayı tokenlarını kaldırır '''
    def remove_punct(inp):
        inp = inp.replace('.', ' ').replace(',', ' ').replace('!', ' ').replace('/', ' ')
        return ''.join([c for c in inp if c not in string.punctuation or (c == '_' and keep_lodash)])
    tweet = unidecode(tweet).replace('\n', ' ')
    tokens = []
    for w in map(lambda x: x.strip(), tweet.split()):
        if w.isnumeric(): continue
        tokens.append(remove_punct(w))
    return ' '.join(tokens).lower()

def load_corpus(inp_path, ngrams=1, min_count=1, threshold=2):
    if not os.path.exists(inp_path):
        raise Exception(f'Verilen adreste json dump bulunamadı -> {inp_path}')

    if ngrams < 1: raise Exception(f'ngrams değişkeni en az 1 (unigram) olmalıdır.')   

    print('Corpus yükleniyor.') 
    corpus = []
    with open(inp_path, encoding='utf-8') as f:
        tweet_objects = json.loads(f.read())

        for item in tweet_objects:
            tweet = item['full_text']
            tweet = preprocess(tweet)
            
            corpus.append(tweet.split()) 

    if ngrams > 1:
        bigram_model = Phrases(corpus, min_count=min_count, threshold=threshold, delimiter=' ')
        bigram_phraser = Phraser(bigram_model)
        for _ in range(ngrams - 1):
            corpus = bigram_phraser[corpus]

    return corpus

def get_similars(base_word, model, topn=100):
    candidates = []
    base_word = preprocess(base_word)
    try:
        for w, sim in model.wv.most_similar(base_word, topn=100):
            d = jf.levenshtein_distance(base_word, w)
            candidates.append({
                'kelime': w,
                'benzerlik': sim,
                'levenshtein': d
            })
    except:
        candidates = []

    return {
        'anahtar_kelime': base_word,
        'oneriler': candidates 
    }